#include "stdafx.h"
#include <iostream>
#include <stdlib.h>
#include <limits>

using namespace System;
using namespace std;

struct nodo {
	int nro;        // en este caso es un numero entero
	struct nodo *sgte, *ant;
};

typedef struct nodo *Tlista;

int tamanioLista(Tlista lista)
{
	int n = 0;
	while (lista != NULL) {
		n++;
		lista = lista->sgte;
	}
	return n;
}

int tamanioLista(Tlista inicio, Tlista ultimo)
{
	int n = 0;
	while (inicio != ultimo->sgte)
	{
		n++;
		inicio = inicio->sgte;
	}
	return n;
}

void insertarInicio(Tlista &lista, int valor)
{
	Tlista nuevo = NULL;
	nuevo = new(struct nodo);
	nuevo->nro = valor;
	nuevo->sgte = NULL;
	nuevo->ant = NULL;

	if (lista == NULL) {
		lista = nuevo;
	}
	else {
		nuevo->sgte = lista;
		lista->ant = nuevo;
		lista = nuevo;
		lista->ant = NULL;
	}


}

void insertarFinal(Tlista &lista, int valor)
{
	Tlista nuevo = NULL;
	nuevo = new(struct nodo);
	nuevo->nro = valor;
	nuevo->sgte = NULL;
	nuevo->ant = NULL;

	if (lista == NULL) {
		lista = nuevo;
	}
	else {
		Tlista p = lista;
		while (p->sgte != NULL) {
			p = p->sgte;
		}

		p->sgte = nuevo;
		nuevo->ant = p;
		nuevo->sgte = NULL;
	}


}

void insertarPosicion(Tlista &lista, int valor, int pos)
{
	if (pos == 1) {
		insertarInicio(lista, valor);
	}
	else {
		if (pos == tamanioLista(lista) + 1) {
			insertarFinal(lista, valor);
		}
		else {
			Tlista nuevo = NULL;
			nuevo = new(struct nodo);
			nuevo->nro = valor;
			nuevo->sgte = NULL;
			nuevo->ant = NULL;

			Tlista p = lista;
			int n = 1;

			while (n != pos) {
				n++;
				p = p->sgte;
			}

			nuevo->sgte = p;
			nuevo->ant = p->ant;
			p->ant = nuevo;
			nuevo->ant->sgte = nuevo;
		}

	}


}

void eliminarInicio(Tlista &lista)
{
	Tlista p = lista;
	if (lista->sgte != NULL) {
		lista = lista->sgte;
		lista->ant = NULL;
	}
	else {
		lista = NULL;
	}
	delete(p);
}

void eliminarFinal(Tlista &lista)
{
	Tlista p = lista;
	if (p->sgte != NULL) {
		while (p->sgte != NULL) {
			p = p->sgte;
		}
		p->ant->sgte = NULL;
	}
	else {
		lista = NULL;
	}
	delete(p);
}

void eliminarPosicion(Tlista &lista, int pos)
{
	if (pos == 1) {
		eliminarInicio(lista);
	}
	else {
		Tlista p = lista;
		int n = 1;
		while (n != pos) {
			p = p->sgte;
			n++;
		}
		if (p->sgte != NULL) {
			p->ant->sgte = p->sgte;
			p->sgte->ant = p->ant;
		}
		else {
			p->ant->sgte = NULL;
		}
		delete(p);
	}

}

void ordenamientoInsercion(Tlista &lista, int valor)
{
	if (lista == NULL) {
		insertarInicio(lista, valor);
	}
	else {
		int n = 1;
		Tlista q = lista;
		while (q->sgte != NULL && valor>q->nro) {
			n++;
			q = q->sgte;
		}
		if (valor>q->nro) {
			n++;
		}
		if (n>tamanioLista(lista)) {
			insertarFinal(lista, valor);
		}
		else {
			insertarPosicion(lista, valor, n);
		}
	}
}

void ordenamientoSeleccion(Tlista lista)
{
	if (tamanioLista(lista) >= 2)
	{
		Tlista p = NULL, xm = NULL;
		int menor;
		while (lista->sgte != NULL)
		{
			p = lista->sgte;
			xm = NULL;
			menor = lista->nro;
			while (p != NULL)
			{
				if (menor>p->nro)
				{
					menor = p->nro;
					xm = p;
				}
				p = p->sgte;
			}
			if (xm != NULL)
			{
				xm->nro = lista->nro;
				lista->nro = menor;
			}
			lista = lista->sgte;
		}
	}
}

void ordenamientoIntercambio(Tlista lista)
{
	if (tamanioLista(lista) >= 2)
	{
		Tlista p; int aux;
		while (lista->sgte != NULL)
		{
			p = lista->sgte;
			while (p != NULL)
			{
				if (p->nro < lista->nro)
				{
					aux = p->nro;
					p->nro = lista->nro;
					lista->nro = aux;
				}
				p = p->sgte;
			}
			lista = lista->sgte;
		}
	}
}

void ordenamientoBurbuja(Tlista lista)
{
	if (tamanioLista(lista) >= 2)
	{
		bool cambios;
		int aux;
		Tlista p = NULL, q = NULL;
		do {
			cambios = false;
			p = lista;
			do {
				if (p->nro > p->sgte->nro)
				{
					aux = p->nro;
					p->nro = p->sgte->nro;
					p->sgte->nro = aux;
					cambios = true;
				}
				p = p->sgte;
			} while (p->sgte != q);
			q = p;
		} while (cambios);
	}
}

void ordenamientoShell(Tlista lista)
{
	int salto = tamanioLista(lista) / 2;
	bool intercambios;

	do {
		intercambios = false;
		Tlista p = lista, q = lista;
		int aux, n = 0;
		do
		{
			n++;
			q = q->sgte;
		} while (n<salto);

		while (q != NULL)
		{
			if (q->nro<p->nro)
			{
				aux = q->nro;
				q->nro = p->nro;
				p->nro = aux;
				intercambios = true;
			}
			p = p->sgte;
			q = q->sgte;
		}
		salto = salto / 2;
	} while (intercambios || salto>0);
}

void quicksort(Tlista inicio, Tlista ultimo)
{
	if (inicio != ultimo)
	{

		Tlista p = inicio;
		Tlista q = ultimo;
		Tlista pivote = inicio;
		bool vaiven = true;

		while (p != q)
		{
			if (vaiven)
			{
				if (q->nro<pivote->nro)
				{
					int aux1 = pivote->nro;
					pivote->nro = q->nro;
					q->nro = aux1;

					pivote = q;
					p = p->sgte;
					vaiven = false;
				}
				else {
					q = q->ant;
				}
			}
			else {
				if (p->nro>pivote->nro)
				{
					int aux2 = pivote->nro;
					pivote->nro = p->nro;
					p->nro = aux2;

					pivote = p;
					q = q->ant;
					vaiven = true;
				}
				else {
					p = p->sgte;
				}
			}
		}
		if (pivote != inicio)
		{
			quicksort(inicio, pivote->ant);
		}
		if (pivote != ultimo)
		{
			quicksort(pivote->sgte, ultimo);
		}
	}
}

void mezcla(Tlista inicio, Tlista medio, Tlista ultimo)
{
	Tlista listaTemporal = NULL;
	Tlista p = inicio;
	Tlista q = medio->sgte;

	while ((p != medio->sgte) && (q != ultimo->sgte))
	{
		if (p->nro<q->nro)
		{
			insertarFinal(listaTemporal, p->nro);
			p = p->sgte;
		}
		else
		{
			insertarFinal(listaTemporal, q->nro);
			q = q->sgte;
		}
	}
	if (p == medio->sgte)
	{
		while (q != ultimo->sgte)
		{
			insertarFinal(listaTemporal, q->nro);
			q = q->sgte;
		}
	}
	else
	{
		while (p != ultimo->sgte)
		{
			insertarFinal(listaTemporal, p->nro);
			p = p->sgte;
		}
	}

	while (inicio != ultimo->sgte)
	{
		inicio->nro = listaTemporal->nro;
		inicio = inicio->sgte;
		listaTemporal = listaTemporal->sgte;
	}
}

void mergesort(Tlista inicio, Tlista ultimo)
{
	if (inicio != ultimo)
	{
		int medio = 1;
		Tlista recorreMedio = inicio;

		while (medio<(tamanioLista(inicio, ultimo)) / 2)
		{
			recorreMedio = recorreMedio->sgte;
			medio++;
		}

		mergesort(inicio, recorreMedio);
		mergesort(recorreMedio->sgte, ultimo);
		mezcla(inicio, recorreMedio, ultimo);

	}
}

void radixMove(Tlista lista, Tlista vector[])
{
	for (int i = 0; i<10; i++)
	{
		while (vector[i] != NULL)
		{
			lista->nro = vector[i]->nro;
			lista = lista->sgte;
			vector[i] = vector[i]->sgte;
		}
	}
}

void radixDelete(Tlista vector[])
{
	Tlista p = NULL;
	for (int i = 0; i<10; i++)
	{
		while (vector[i] != NULL)
		{
			p = vector[i];
			vector[i] = vector[i]->sgte;
			delete(p);
		}
	}
}

void radixSort(Tlista lista, Tlista vector[])
{
	if (tamanioLista(lista) >= 2)
	{
		Tlista p = NULL;
		int part = 10, div = 1, cifra;
		bool interruptor;
		do {
			p = lista;
			interruptor = false;
			while (p != NULL)
			{
				cifra = p->nro / div;
				if (cifra != 0)
					interruptor = true;
				cifra = cifra%part;
				insertarFinal(vector[cifra], p->nro);
				p = p->sgte;
			}
			radixMove(lista, vector);
			radixDelete(vector);
			div = div * 10;
		} while (interruptor);
	}
}

void mostrarLista(Tlista lista)
{
	if (lista == NULL) {
		cout << "\n\tLista vacia!" << endl;
	}
	else
	{
		int n = 0;
		while (lista != NULL)
		{
			cout << ' ' << n + 1 << ") " << lista->nro << endl;
			lista = lista->sgte;
			n++;
		}
	}

}

void menu1()
{
	cout << "\n\t\tMETODOS LISTA ENLAZADA DOBLE\n\n";
	cout << " 01. INSERTAR AL INICIO               " << endl;
	cout << " 02. INSERTAR AL FINAL                " << endl;
	cout << " 03. INSERTAR EN UNA POSICION         " << endl;
	cout << " 04. ELIMINAR EL PRIMER ELEMENTO      " << endl;
	cout << " 05. ELIMINAR ULTIMO ELEMENTO         " << endl;
	cout << " 06. ELIMINAR ELEMENTO POR POSICION   " << endl;
	cout << " 07. METODO ORDENAMIENTO INSERCION    " << endl;
	cout << " 08. METODO ORDENAMIENTO SELECCION    " << endl;
	cout << " 09. METODO ORDENAMIENTO INTERCAMBIO  " << endl;
	cout << " 10. METODO ORDENAMIENTO BURBUJA      " << endl;
	cout << " 11. METODO ORDENAMIENTO SHELL        " << endl;
	cout << " 12. METODO ORDENAMIENTO QUICK        " << endl;
	cout << " 13. METODO ORDENAMIENTO MERGE        " << endl;
	cout << " 14. METODO ORDENAMIENTO RADIX        " << endl;
	cout << " 15. MOSTRAR LISTA                    " << endl;
	cout << "(0). SALIR                            " << endl;

	cout << "\n INGRESE OPCION: ";
}

/*                        Funcion Principal
---------------------------------------------------------------------*/

int main()
{
	Tlista lista = NULL;
	Tlista inicio = NULL;
	Tlista ultimo = NULL;
	Tlista recorre = NULL;
	int op;     // opcion del menu
	int _dato;  // elemenento a ingresar
	int pos;    // posicion a insertar

	Tlista vector[10];
	for (int i = 0; i<10; i++)
		vector[i] = NULL;

	system("color 0b");

	do
	{
		menu1();  cin >> op;

		switch (op)
		{
		case 1:

			cout << "\n NUMERO A INSERTAR: "; cin >> _dato;
			insertarInicio(lista, _dato);
			mostrarLista(lista);

			break;

		case 2:

			cout << "\n NUMERO A INSERTAR: "; cin >> _dato;
			insertarFinal(lista, _dato);
			mostrarLista(lista);

			break;

		case 3:

			cout << "\n NUMERO A INSERTAR: "; cin >> _dato;
			cout << " Posicion : ";       cin >> pos;
			while (pos <= 0 || pos >= tamanioLista(lista) + 1) {
				cout << "\n\t-->LA POSICION NO EXISTE!, ingrese una posicion correcta..." << endl;
				cout << " Posicion : ";       cin >> pos;
			}
			insertarPosicion(lista, _dato, pos);
			mostrarLista(lista);

			break;

		case 4:

			if (lista == NULL) {
				cout << "\n\t\t||Operacion invalida!||\n\n";
			}
			else {
				eliminarInicio(lista);
				cout << "\n PRIMER ELEMENTO ELIMINADO..." << endl;
			}
			mostrarLista(lista);

			break;

		case 5:

			if (lista == NULL) {
				cout << "\n\t\t||Operacion invalida!||\n\n";
			}
			else {
				eliminarFinal(lista);
				cout << "\n ULTIMO ELEMENTO ELIMINADO..." << endl;
			}
			mostrarLista(lista);

			break;

		case 6:

			if (lista == NULL) {
				cout << "\n\t\t||Operacion invalida!||\n\n";
			}
			else {
				cout << " Posicion : ";          cin >> pos;
				while (pos <= 0 || pos >= tamanioLista(lista) + 1) {
					cout << "\n\t-->LA POSICION NO EXISTE!, ingrese una posicion correcta..." << endl;
					cout << " Posicion : ";       cin >> pos;
				}
				eliminarPosicion(lista, pos);
			}
			mostrarLista(lista);

			break;

		case 7:

			cout << "INSERTAR ELEMETO: ";       cin >> _dato;
			ordenamientoInsercion(lista, _dato);
			mostrarLista(lista);

			break;

		case 8:
			if (lista != NULL)
			{
				ordenamientoSeleccion(lista);
			}
			mostrarLista(lista);

			break;

		case 9:
			if (lista != NULL)
			{
				ordenamientoIntercambio(lista);
			}
			mostrarLista(lista);

			break;

		case 10:
			if (lista != NULL)
			{
				ordenamientoBurbuja(lista);
			}
			mostrarLista(lista);

			break;

		case 11:
			if (lista != NULL)
			{
				ordenamientoShell(lista);
			}
			mostrarLista(lista);

			break;

		case 12:
			if (lista != NULL)
			{
				inicio = lista;
				recorre = lista;
				while (recorre->sgte != NULL)
				{
					recorre = recorre->sgte;
				}
				ultimo = recorre;
				quicksort(inicio, ultimo);
			}
			mostrarLista(lista);

			break;

		case 13:
			if (lista != NULL)
			{
				inicio = lista;
				recorre = lista;
				while (recorre->sgte != NULL)
				{
					recorre = recorre->sgte;
				}
				ultimo = recorre;
				mergesort(inicio, ultimo);
			}
			mostrarLista(lista);

			break;

		case 14:
			if (lista != NULL)
			{
				radixSort(lista, vector);
			}
			mostrarLista(lista);

			break;

		case 15:

			cout << "\n\n MOSTRANDO LISTA\n\n";
			mostrarLista(lista);

			break;
		default:
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			cout << "\nOPCION INCORRECTA!";

		}

		cout << endl << endl;
		system("pause");  system("cls");

	} while (op != 0);


	system("pause");
	return 0;
}
